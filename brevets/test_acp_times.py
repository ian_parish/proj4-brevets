"""
Test for brevet calculator acp_times methods
"""

import acp_times


def test_goodIn_open():
    """
    Tests a valid input for open_time
    """
    control = 150
    brevet = 200
    time = "2020-01-01 00:00"

    assert acp_times.open_time(control, brevet, time) == "2020-01-01T04:25:00+00:00"


def test_goodIn_close():
    """
    Tests a valid input for close_time
    """
    control = 150
    brevet = 200
    time = "2020-01-01 00:00"

    assert acp_times.close_time(control, brevet, time) == "2020-01-01T10:00:00+00:00"


def test_controlLonger():
    """
    Tests for when the control is too far beyond the length of the brevet
    """
    control = 1200
    brevet = 600
    time = "2020-01-01 00:00"

    assert acp_times.close_time(control, brevet, time) == -1


def test_controlEqBrev_open():
    """
    Tests the occasion when the control location is the same length as brevet
    """
    control = 600
    brevet = 600
    time = "2020-01-01 00:00"

    assert acp_times.open_time(control, brevet, time) == "2020-01-01T18:48:00+00:00"
    

def test_controlEqBrev_close():
    control = 600
    brevet = 600
    time = "2020-01-01 00:00"

    assert acp_times.close_time(control, brevet, time) == "2020-01-02T16:00:00+00:00"