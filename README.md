# Project 4: Brevet Time Calculator
### Author: Ian Parish Email: iparish@cs.uoregon.edu
### Developed for CIS322 F20, University of Oregon

---

This is an implementation of a calculator used to determine the opening and closing times of control points as determined by the user for a brevet for _randonneurs_ (see (https://en.wikipedia.org/wiki/Randonneuring) for more infomation on randonneuring). Its implementation is based on the algorithm outlined at (https://rusa.org/pages/acp-brevet-control-times-calculator), and the rules established at the *Randonneurs USA* website (https://rusa.org/pages/rulesForRiders).

---

## Requirements:
 * Docker
 * a valid credentials.ini file
Yep, that's all you need!

---

## Testing:
A suite of Nose tests for Python is included. See usage below.

Usage: `> nosetests test_acp_times.py`